# Introduction to On-board Software - Slides


Series of slides giving a short introduction to the topic of On-board software for satellites.


---

## The Slides Series

### Spanish version 

|   |   |
|---|---|
| Parte I     | - [Introducción](https://olmanqj.gitlab.io/intro-obsw-slides/1_introduction_es.html)|
| Parte II    | - [Funciones del On-board Software](https://olmanqj.gitlab.io/intro-obsw-slides/2_obsw_functions_es.html)|
| Parte III   | - [Arquitectura del On-board Software](https://olmanqj.gitlab.io/intro-obsw-slides/3_obsw_arch_es.html)|
| Parte IV    | - Comunicaciones (*planned*) |
| Parte V     | - Telemetria & Telecomandos (*planned*) |
| Parte VI    | - Detección, aislamiento y recuperación de fallas (*planned*)  |
| Parte VII   | - Proceso & tecnologías de desarrollo (*planned*) |
| Parte VIII  | - Verificación & Validación (*planned*) |
| Parte IX    | - Conceptos Avanzados (*planned*) |

--- 

### English version

|   |   |
|---|---|
| Part I     | - Introduction (*planned*) |
| Part II    | - On-board Software Functions (*planned*) |
| Part III   | - On-board Software Architecture (*planned*) |
| Part IV    | - Communications (*planned*) |
| Part V     | - Telemetry & Telecommands (*planned*) |
| Part VI    | - Fault detection, isolation, and recovery (*planned*) |
| Part VII   | - Development Process & Technologies (*planned*) |
| Part VIII  | - Verification & Validation (*planned*) |
| Part IX    | - Advanced Concepts (*planned*) |



--- 

### Tools
Slides generated from Markdown using [backslide](https://github.com/sinedied/backslide)


### Run locally

~~~
npm install -g backslide

bs serve
~~~

### Export to pdf
Extra dependency decktape needed.
~~~
npm install -g decktape

bs pdf 
~~~
